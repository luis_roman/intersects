estado<-"df"
street1 <- "reforma"
street2 <- "insurgentes"

ogrListLayers(paste0('./shiny/data/',estado,"_eje_vial.shp")) ## will show you available layers for the above dataset
shape=readOGR(paste0('./shiny/data/',estado,"_eje_vial.shp"), layer=paste(estado,"_eje_vial",sep='')) ## will load the shapefile to your dataset.

intersect <- function(street1, street2, shape){
    res <- list()
    av<-shape@data
    ## Georeferenciacion de Tuits
    ## https://twitter.com/ciudad_segura/status/601478017563697153
    ## Choque en Canal de San Juan y Sur 14, Col. Agrícola Oriental, Iztacalco. Afecta de dos carriles al sur 


    ## Nombres de las calles
    nombre1<-toupper(street1)
    nombre2<-toupper(street2) ## "19019" San Pedro Garza
    ## vec1<-which(gregexpr(pattern=nombre1,av$NOMVIAL)>0 & gregexpr(pattern="19019",av$CVEGEO)>0)
    ## vec2<-which(gregexpr(pattern=nombre2,av$NOMVIAL)>0 & gregexpr(pattern="19019",av$CVEGEO)>0)
    vec1<-which(gregexpr(pattern=nombre1,av$NOMVIAL)>0)
    vec2<-which(gregexpr(pattern=nombre2,av$NOMVIAL)>0)
    ## Avenida1
    A<-c()
    for (i in 1:length(vec1)){
        A<-rbind(A,cbind(shape@lines[[vec1[i]]]@Lines[[1]]@coords,matrix(i,dim(shape@lines[[vec1[i]]]@Lines[[1]]@coords)[1],1)))
    }
    ## Avenida2
    B<-c()
    for (i in 1:length(vec2)){
        B<-rbind(B,cbind(shape@lines[[vec2[i]]]@Lines[[1]]@coords,matrix(i,dim(shape@lines[[vec2[i]]]@Lines[[1]]@coords)[1],1)))
    }
    ## Corregimos el formato de [Lat,Lon,indice]
    A<-data.frame(cbind(A[,2],A[,1],A[,3]))
    B<-data.frame(cbind(B[,2],B[,1],B[,3]))
    colnames(A)<-cbind("Lat","Lon","Indice")
    colnames(B)<-cbind("Lat","Lon","Indice")
    ## pivote es el conjunto de puntos desde el cual se buscara la menor distancia
    pivote<-which.min(c(dim(A)[1],dim(B)[1]))[1]
    ## El conjunto de puntos con menor cardinalidad se llamara X, el otro sera Y
    if (pivote == 1){
        X<-A
        Y<-B
    }else{
        X<-B
        Y<-A
    }

    aux<-matrix(0,dim(X)[1],2)
    for (i in 1:dim(X)[1]){
        aux[i,1]<-min(as.matrix(distHaversine(X[i,c(2,1)],Y[,c(2,1)], r=6378137)))
        aux[i,2]<-which.min(as.matrix(distHaversine(X[i,c(2,1)],Y[,c(2,1)], r=6378137)))[1]
    }

    res[[1]] <- X[which.min(aux[,1]),]
    Y[aux[which.min(aux[,1]),2],]
    ## Distancia minima entre conjuntos
    res[[2]] <- distHaversine(X[which.min(aux[,1]),][,c(2,1)],Y[aux[which.min(aux[,1]),2],][,c(2,1)], r=6378137)
    res
}










